## App Structure

This app is fairly simple. It uses firebase, a cross platform object store 
that's fast, and very easy to use. 

You'll find screens in the `/containers` folder, and reusable components in 
the `/components` folder.

## Coding Standards

The app is coded using typescript. We use vscode and intellij for development
but obviously, you're free to use the ide (or not) of your choice. Please try
not to commit ide configuration files into the repository,

### tslint

This project contains tslint rules that enforce the bulk of our 
coding standards. Configure your ide to use them, and please don't 
commit code that fails linting. 

### Modules and classes 

Modules (exported functions) should have an export statement 
at the end of the file. They should be named in lower camel case 
(e.g. `someModule.ts`).

Classes should be named in upper camel case (e.g. `SomeClass`) and only 
export one thing, the class itself. E.g. `export class SomeClass`.

Please don't use default exports.  

The idea is to introduce some basic discoverability. `MyClass.ts` should be created 
with `new MyClass()`. When using `myModule.ts` you might need to open the file and 
scroll to the bottom to find an export statement, although intellij and vscode do 
a pretty good job of auto importing modules with named exports. 

## Troubleshooting the build

[If you're using firebase and realm together.](https://github.com/realm/realm-js/issues/1607) 

If you see odd errors relating to derived data and RNFS, add the library as a target dependency. 