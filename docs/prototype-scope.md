# App requirements

React Native cross platform (iPhone/Android) app.

## Main tech

React Native, TypeScript, (Firebase)

## First stage - prototype

### SCREEN 1

Displays a scrollable activity log and Add Entry button.

Add Entry button opens the second screen where user can add their mood entry.

Activity log section is scrollable (only data scrolls, Self, Others, General stay permanent) and displays how user feels on a cetrain day.

Latest entries comes first. If there is no entry on a day we don't need to display it.

User can click and see different lists depending on 3 options: Self, Others, General.

Each record consists of 4 feelings. Different icons will represent different levels of mood. For example from 0 - Sad to 10 Sad. We will provide icons. Also each record can have text up to 100 characters.

### SCREEN 2

Mood entry.

Using 3 tabs (Self, Others, General) user can choose levels of Glad, Mad, Sad, Anxious for Self, Others and General.

Every category (for example Glad) will have 10 levels from 1 to 10.
Icon to the left of slider will represent choosen level. We will provede 10 icons per category.

At the bottom of the screen user can optionally type in some text describing their mood. One text field per tab.

Add entry will save the entry and return to 1st screen.

1st screen activity log should be updated.

### GENERAL

At the first stage we are looking mostly for UI implementation.
Don't worry about linking UI to a Firebase or any other data store.
Just pass the data between screens for now.
