import * as React from 'react';
import { Navigation } from 'react-native-navigation';
import { ActivityLog } from './containers/ActivityLog';
import Welcome from './containers/Welcome';

function sceneCreator(sceneComp) {
  return () => {
    return class Wrapper extends React.Component {

      /*
       * We need to pass down the static options property so that react-native-navigation
       * can see it.
       */
      static options = {
        ...sceneComp.options,
      };

      render() {
        return React.createElement(sceneComp, this.props);
      }
    };
  };
}

/*
 * All Containers must be registered here.
 */
(Navigation as any).registerComponent('Welcome', sceneCreator(Welcome));
(Navigation as any).registerComponent('ActivityLog', sceneCreator(ActivityLog));

/*
 * The initial application stack.
 */
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'Welcome',
            },
          },
        ],
      },
    },
  });
});