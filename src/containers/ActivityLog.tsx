import * as React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RNFirebase } from 'react-native-firebase';
import { OutlineButton } from '../components/OutlineButton';
import * as activity from '../service/activity';

interface Props {
  componentId: string;
}

interface State {
  data: RNFirebase.firestore.QuerySnapshot | null;
}

export class ActivityLog extends React.Component<Props, State> {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    this.loadActivities();
  }

  loadActivities = async (): Promise<void> => {
    const results = await activity.findAll();
    this.setState({
      data: results,
    });
  };

  createActivity = async (): Promise<void> => {
    await activity.createActivity({});

    // Refresh.
    return this.loadActivities();
  };

  render() {
    const activityCount = (this.state.data) ? this.state.data.size : 0;
    return (
      <View style={styles.view}>
        <Text>Hello! {activityCount} Activities Saved</Text>
        <OutlineButton
          label='Create Activity'
          style={{ marginTop: 10 }}
          onPress={this.createActivity}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
