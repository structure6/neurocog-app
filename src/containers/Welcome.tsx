import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import firebase from 'react-native-firebase';
import { Navigation } from 'react-native-navigation';

import * as Spinner from 'react-native-spinkit';

interface Props {
  componentId: string;
}

interface State {
  isLoggedIn: boolean;
}

export default class Welcome extends React.Component<Props, State> {

  unsubscriber;

  constructor(props) {
    super(props);

    this.state = {
      isLoggedIn: false,
    };
  }

  static get options() {
    return {
      topBar: {
        visible: false,
      },
    };
  }

  didLogin = async () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: 'ActivityLog',
              },
            },
          ],
        },
      },
    });
  };

  componentDidMount() {
    this.unsubscriber = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.didLogin();
      } else {
        firebase.auth().signInAnonymouslyAndRetrieveData()
          .then((loggedInAs) => {
            if (!loggedInAs) {
              throw new Error('Unable to authenticate to firebase.');
            }
          });
      }
    });
  }

  componentWillUnmount() {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
  }

  render() {
    return (
      <View style={styles.view}>
        <Spinner type='Bounce' size={46}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
});
