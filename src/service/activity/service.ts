import firebase, { RNFirebase } from 'react-native-firebase';
import { Activity } from './types';

const db: RNFirebase.firestore.Firestore = firebase.firestore();

function findAll(): Promise<RNFirebase.firestore.QuerySnapshot> {
  return db.collection('activities').get();
}

async function createActivity(activity: Activity): Promise<Activity> {
  const toSave: Activity = { date: new Date(), ...activity };

  await db.collection('activities')
    .doc()
    .set(activity);

  return toSave;
}

export {
  createActivity,
  findAll,
};
