interface Activity {
  uuid?: string;
  date?: Date;
  feeling?: string;
  glad?: number;
  mad?: number;
  sad?: number;
  anxious?: number;
}

export {
  Activity,
};
