import * as React from 'react';
import { StyleSheet, Text, TouchableHighlight } from 'react-native';

interface Props {
  label?: string;
  onPress?: Function;
  style?: any;
}

interface State {
  active: boolean;
}

export class OutlineButton extends React.Component<Props, State> {

  constructor(props) {
    super(props);

    this.state = {
      active: false,
    };
  }

  onPress = () => {
    if (this.props.onPress) {
      this.props.onPress();
    }
  };

  render() {
    return (
      <TouchableHighlight
        style={[styles.button, this.props.style]}
        underlayColor='#fff'
        onPress={this.onPress}
      >
        <Text style={styles.text}>
          {this.props.label}
        </Text>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#000',
    paddingHorizontal: 20,
    paddingVertical: 5,
    justifyContent: 'center',
  },
  text: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000',
  },
});
